# Factorio Mod: Dorfl's Early Game

*A gentle step up from vanilla during the early game.*

WIP

Something more challenging than a vanilla run yet less mind-boggling than a bobs/angels/pyanodon marathon... for when your brain can't handle either extreme after a day at work :-)

## Vague aims

* Stay simple. Be a tightly integrated Vanilla++
* Make water sources more useful
* Increase copper and stone usage in the iron-heavy early game
* Stick fairly close to vanilla tech, but make recipes slightly more complex
* Try to avoid tiers. The MK2/3/4...N grind isn't very interesting

## Compatibility

Intended for a fresh mostly-vanilla game with other mods that don't modify vanilla production chains too much, but does still load up ok with bobs/angels turned on (for example).

## Resources

* Sludge (dredging)
* Biomass --> Gasket
* Sand --> Glass, Ceramic substrate
* Gold nugget --> Gold foil

## Materials

* Ceramic substrate --> Green circuit, Button Cell
* Glass plate --> Science flasks, Car, Rail signals, Solar Panel, Lamp, Electric Furnace, Night Vision
* Gold foil --> Blue circuit, Low density structure

## Intermediate Products

* Glass flask --> All science packs
* Gasket --> Most things using fluids and/or pipes
* Copper bushing --> Most things with moving parts
* Button cell --> Combinators, Red circuit, Rail signals

## Machines

* Offshore dredge
* Hydrocyclone (solid-liquid separation) --> Biomass, Sand, Gold

## Technologies

* Gold processing

