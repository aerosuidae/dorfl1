local function merge(b, a)
	for k, v in pairs(b) do
		a[k] = v
	end
	return a
end

local function dataItem(proto)
	local name = proto.name
	data:extend({
		merge(proto, {
			type = 'item',
			icon = '__dorfl1__/graphics/'..name..'.png',
			icon_size = 32,
			stack_size = 1,
			order = 'z['..name..']',
		}),
	})
end

local function dataFluid(proto)
	local name = proto.name
	data:extend({
		merge(proto, {
			type = 'fluid',
			icon = '__dorfl1__/graphics/'..name..'.png',
			icon_size = 32,
			stack_size = 1,
			default_temperature = 25,
			heat_capacity = '1KJ',
			base_color = {r=132/255, g=176/255, b=11/255},
			flow_color = {r=132/255, g=176/255, b=11/255},
			max_temperature = 100,
			pressure_to_speed_ratio = 0.4,
			flow_to_energy_ratio = 0.59,
			order = 'z['..name..']',
		}),
	})
end

local function dataRecipe(proto)
	local name = proto.name
	data:extend({
		merge(proto, {
			type = 'recipe',
			icon = '__dorfl1__/graphics/'..name..'.png',
			icon_size = 32,
			enabled = false,
			hidden = false,
			energy_required = 0.5,
			category = 'crafting',
			order = 'z['..name..']',
		}),
	})
end

local function dataTech(proto)
	local name = proto.name
	data:extend({
		merge(proto, {
			type = 'technology',
			name = name,
			icon = '__dorfl1__/graphics/'..name..'.png',
			icon_size = 128,
			effects = {},
			prerequisites = {},
			unit = {
				count = 10,
				ingredients = {
					{'automation-science-pack', 1},
				},
				time = 15
			},
			order = 'z',
		}),
	})
end

local function stack(name, count)
	return {
		type = 'item',
		name = name,
		amount = count or 1,
	}
end

local function fluid(name, count)
	return {
		type = 'fluid',
		name = name,
		amount = count or 1,
	}
end

local plateEnergy = data.raw.recipe['iron-plate'].energy_required
	or data.raw.recipe['iron-plate'].normal.energy_required
	or 3.2

local plateEnergy2 = data.raw.recipe['steel-plate'].energy_required
	or data.raw.recipe['steel-plate'].normal.energy_required
	or 3.2

dataFluid{
	name = 'sludge',
	order = 'c[sludge]',
	base_color = {r = 0.7, g = 0.4, b = 0.1},
	flow_color = {r = 0.7, g = 0.4, b = 0.1},
}

dataItem{
	name = 'copper-bushing',
	subgroup = 'intermediate-product',
	order = 'c[copper-bushing]',
	stack_size = 100,
}

dataItem{
	name = 'gasket',
	subgroup = 'intermediate-product',
	order = 'c[gasket]',
	stack_size = 100,
}

dataItem{
	name = 'sand',
	subgroup = 'raw-resource',
	order = 'f[sand]',
	stack_size = 100,
}

dataItem{
	name = 'biomass',
	subgroup = 'raw-resource',
	order = 'f[biomass]',
	stack_size = 100,
	fuel_value = '1MJ',
	fuel_category = 'chemical',
}

dataItem{
	name = 'glass-plate',
	subgroup = 'raw-material',
	order = 'f[glass-plate]',
	stack_size = 100,
}

dataItem{
	name = 'glass-flask',
	subgroup = 'science-pack',
	order = 'a[[glass-flask]]',
	stack_size = 100,
}

dataItem{
	name = 'ceramic-substrate',
	subgroup = 'raw-material',
	order = 'f[ceramic-substrate]',
	stack_size = 100,
}

dataItem{
	name = 'gold-nugget',
	subgroup = 'raw-resource',
	order = 'f[gold-nugget]',
	stack_size = 1000,
}

dataItem{
	name = 'gold-foil',
	subgroup = 'raw-material',
	order = 'f[gold-foil]',
	stack_size = 100,
}

dataRecipe{
	name = 'copper-bushing',
	subgroup = 'intermediate-product',
	order = 'c[copper-bushing]',
	enabled = true,
	ingredients = {
		stack('copper-plate', 1),
	},
	results = {
		stack('copper-bushing', 2),
	},
}

dataRecipe{
	name = 'gasket',
	subgroup = 'intermediate-product',
	order = 'c[gasket]',
	enabled = true,
	ingredients = {
		stack('biomass', 1),
	},
	results = {
		stack('gasket', 1),
	},
}

data:extend({
	{
		type = 'recipe-category',
		name = 'solid-liquid-separation',
	}
})

dataRecipe{
	name = 'sand-separation',
	subgroup = 'raw-resource',
	order = 'f[sand]',
	category = 'solid-liquid-separation',
	energy_required = 1,
	enabled = true,
	ingredients = {
		fluid('sludge', 100),
	},
	results = {
		stack('sand', 2),
	},
}

dataRecipe{
	name = 'biomass-separation',
	subgroup = 'raw-resource',
	order = 'f[biomass]',
	category = 'solid-liquid-separation',
	energy_required = 3,
	enabled = true,
	ingredients = {
		fluid('sludge', 500),
	},
	results = {
		stack('biomass', 2),
	},
}

dataTech{
	name = 'gold-processing',
	effects = {
		{ type = 'unlock-recipe', recipe = 'gold-separation' },
		{ type = 'unlock-recipe', recipe = 'gold-foil' },
	},
	prerequisites = {
		'chemical-science-pack',
	},
	unit = {
		count = 100,
		ingredients = {
			{'automation-science-pack', 1},
			{'logistic-science-pack', 1},
			{'chemical-science-pack', 1},
		},
		time = 30
	},
}

dataRecipe{
	name = 'gold-separation',
	subgroup = 'raw-resource',
	order = 'f[gold]',
	category = 'solid-liquid-separation',
	energy_required = 30,
	enabled = false,
	ingredients = {
		fluid('sludge', 30000),
	},
	results = {
		stack('gold-nugget', 1),
	},
}

dataRecipe{
	name = 'gold-foil',
	subgroup = 'raw-material',
	order = 'f[gold-foil]',
	category = 'smelting',
	enabled = false,
	energy_required = plateEnergy*5,
	ingredients = {
		stack('gold-nugget', 1),
	},
	results = {
		stack('gold-foil', 10),
	},
}

dataRecipe{
	name = 'biomass-wood',
	subgroup = 'raw-resource',
	order = 'f[biomass2]',
	enabled = true,
	ingredients = {
		stack('wood', 2),
	},
	results = {
		stack('biomass', 1),
	},
}

dataRecipe{
	name = 'glass-plate',
	subgroup = 'raw-material',
	order = 'f[glass-plate]',
	category = 'smelting',
	enabled = true,
	energy_required = plateEnergy*2,
	ingredients = {
		stack('sand', 2),
	},
	results = {
		stack('glass-plate', 1),
	},
}

dataRecipe{
	name = 'glass-flask',
	subgroup = 'science-pack',
	order = 'a[[glass-flask]]',
	category = 'crafting',
	enabled = true,
	energy_required = 1,
	ingredients = {
		stack('glass-plate', 2),
	},
	results = {
		stack('glass-flask', 1),
	},
}

dataRecipe{
	name = 'ceramic-substrate',
	subgroup = 'raw-material',
	order = 'f[ceramic-substrate]',
	enabled = true,
	energy_required = plateEnergy/2,
	ingredients = {
		stack('stone', 1),
		stack('sand', 1),
	},
	results = {
		stack('ceramic-substrate', 1),
	},
}

dataItem{
	name = 'offshore-dredge',
	icon = '__dorfl1__/graphics/offshore-dredge-icon.png',
	subgroup = 'extraction-machine',
	order = 'b[fluids]-b[offshore-pump]',
	stack_size = 10,
	place_result = 'offshore-dredge',
}

dataRecipe{
	name = 'offshore-dredge',
	icon = '__dorfl1__/graphics/offshore-dredge-icon.png',
	subgroup = 'extraction-machine',
	order = 'b[fluids]-b[offshore-pump]',
	energy_required = 3,
	enabled = true,
	ingredients = {
		stack('electronic-circuit', 2),
		stack('iron-gear-wheel', 1),
		stack('pipe', 1),
	},
	results = {
		stack('offshore-dredge', 1),
	},
}

local dredge = table.deepcopy(data.raw['offshore-pump']['offshore-pump'])

dredge.name = 'offshore-dredge'
dredge.minable.result = 'offshore-dredge'
dredge.fluid = 'sludge'
dredge.fluid_box.filter = 'sludge'
dredge.pumping_speed = 600/60

data:extend({dredge})

dataItem{
	name = 'hydrocyclone',
	icon = '__dorfl1__/graphics/hydrocyclone-icon.png',
	subgroup = 'production-machine',
	order = 'b[fluids]-b[hydrocyclone]',
	stack_size = 10,
	place_result = 'hydrocyclone',
}

dataRecipe{
	name = 'hydrocyclone',
	icon = '__dorfl1__/graphics/hydrocyclone-icon.png',
	subgroup = 'production-machine',
	order = 'b[fluids]-b[hydrocyclone]',
	energy_required = 3,
	enabled = true,
	ingredients = {
		stack('iron-plate', 5),
		stack('electronic-circuit', 5),
		stack('pipe', 5),
	},
	results = {
		stack('hydrocyclone', 1),
	},
}

local function animation(name, tiles)
	return {
		filename = '__dorfl1__/graphics/'..name..'-frames-lr.png',
		width = tiles*48,
		height = tiles*48,
		frame_count = 30,
		line_length = 5,
		shift = {0, 0},
		animation_speed = 1.0,
		hr_version = {
			filename = '__dorfl1__/graphics/'..name..'-frames-hr.png',
			width = tiles*96,
			height = tiles*96,
			frame_count = 30,
			line_length = 5,
			shift = {0, 0},
			animation_speed = 1.0,
			scale = 0.5,
		},
	}
end

local hydrocyclone = table.deepcopy(data.raw['assembling-machine']['assembling-machine-2'])

hydrocyclone.name = 'hydrocyclone'
hydrocyclone.icon = '__dorfl1__/graphics/hydrocyclone-icon.png'
hydrocyclone.icon_size = 32
hydrocyclone.minable.result = 'hydrocyclone'
hydrocyclone.next_upgrade = nil
hydrocyclone.crafting_categories = { 'solid-liquid-separation' }
hydrocyclone.crafting_speed = 1
hydrocyclone.animation = animation('hydrocyclone', 3)
hydrocyclone.fluid_boxes[1].pipe_picture = hydrocyclone.fluid_boxes[1].pipe_covers

data:extend({hydrocyclone})

dataItem{
	name = 'button-cell',
	subgroup = 'intermediate-product',
	order = 'h[button-cell]',
	stack_size = 100,
}

dataRecipe{
	name = 'button-cell',
	subgroup = 'raw-material',
	order = 'h[button-cell]',
	energy_required = 3,
	ingredients = {
		stack('ceramic-substrate', 1),
		stack('iron-plate', 1),
		stack('copper-plate', 1),
	},
	results = {
		stack('button-cell', 4),
	},
}
