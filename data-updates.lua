local intermediates = {
	'copper-bushing',
	'gasket',
	'sand-separation',
	'gold-separation',
	'biomass-wood',
	'biomass-separation',
	'glass-plate',
	'glass-flask',
	'ceramic-substrate',
	'gold-foil',
	'button-cell',
}

for _, item in ipairs(intermediates) do
	for mitem, mproto in pairs(data.raw.module) do
		if type(mproto.category) == "string" and mproto.category == "productivity" then
			table.insert(mproto.limitation, item)
		end
	end
end

local function prefixed(str, start)
	return str:sub(1, #start) == start
end

local function suffixed(str, ending)
	return ending == "" or str:sub(-#ending) == ending
end

local function addMinable(entity, item, cmin, cmax)
	local proto = data.raw['simple-entity'][entity]
	if proto and proto.minable then
		local results = proto.minable.results or {}
		table.insert(results, {
			name = item,
			amount_min = cmin,
			amount_max = cmax,
		})
		proto.minable.results = results
	end
end

addMinable('rock-huge', 'sand', 24, 50)
addMinable('rock-big', 'sand', 9, 24)
addMinable('sand-rock-huge', 'sand', 24, 50)
addMinable('sand-rock-big', 'sand', 9, 24)

-- Other rocks, such as from Alien Biomes
for rock, proto in pairs(data.raw['simple-entity']) do
	if prefixed(rock, 'rock-huge-') or prefixed(rock, 'sand-rock-huge-') then
		addMinable(rock, 'sand', 24, 50)
	end
	if prefixed(rock, 'rock-big-') or prefixed(rock, 'sand-rock-big-') then
		addMinable(rock, 'sand', 9, 24)
	end
end

local function isType(rname, tname)
	return data.raw[tname] ~= nil and data.raw[tname][rname] ~= nil or false
end

local function isElectric(rname, tname)
	return (
		isType(rname, tname)
		and data.raw[tname][rname].energy_source
		and data.raw[tname][rname].energy_source.type == 'electric'
	) or false
end

local function itemCount(ingredients, iname)
	for _, ingredient in pairs(ingredients) do
		if (ingredient.name or ingredient[1]) == iname then
			return (ingredient.amount or ingredient[2] or 0)
		end
	end
	return 0
end

local function countIngredient(rname, iname)
	local count = 0
	local recipe = data.raw.recipe[rname]
	if recipe then
		if recipe.ingredients then
			count = itemCount(recipe.ingredients, iname)
		end
		if count == 0 and recipe.normal and recipe.normal.ingredients then
			count = itemCount(recipe.normal.ingredients, iname)
		end
		if count == 0 and recipe.expensive and recipe.expensive.ingredients then
			count = itemCount(recipe.expensive.ingredients, iname)
		end
	end
	return count or 0
end

local function addIngredient(rname, iname, icount)
	local recipe = data.raw.recipe[rname]
	if recipe then
		if recipe.ingredients and itemCount(recipe.ingredients, iname) == 0 then
			table.insert(recipe.ingredients, { iname, icount })
		end
		if recipe.normal and recipe.normal.ingredients and itemCount(recipe.normal.ingredients, iname) == 0 then
			table.insert(recipe.normal.ingredients, { iname, icount })
		end
		if recipe.expensive and recipe.expensive.ingredients and itemCount(recipe.expensive.ingredients, iname) == 0 then
			table.insert(recipe.expensive.ingredients, { iname, icount * 2 })
		end
	end
end

local function subIngredient(rname, iname, icount)
	local recipe = data.raw.recipe[rname]
	local function sub(ingredients, icount)
		for i, ingredient in pairs(ingredients) do
			if (ingredient.name or ingredient[1]) == iname then
				if (ingredient.amount or ingredient[2]) <= icount then
					table.remove(ingredients, i)
				else
					if ingredient.amount then
						ingredient.amount = ingredient.amount - icount
					end
					if ingredient[2] then
						ingredient[2] = ingredient[2] - icount
					end
				end
			end
		end
	end
	if recipe then
		if recipe.ingredients then
			sub(recipe.ingredients, icount)
		end
		if recipe.normal and recipe.normal.ingredients then
			sub(recipe.normal.ingredients, icount)
		end
		if recipe.expensive and recipe.expensive.ingredients then
			sub(recipe.expensive.ingredients, icount*2)
		end
	end
end

local function repIngredient(rname, iname, jname)
	local count = countIngredient(rname, iname)
	if count > 0 then
		subIngredient(rname, iname, count)
		addIngredient(rname, jname, count)
	end
end

for rname, recipe in pairs(data.raw.recipe) do

	local count = countIngredient(rname, 'iron-gear-wheel')
	if count > 0 and not isType(rname, 'tool') then
		addIngredient(rname, 'copper-bushing', math.max(1, math.ceil(count/2)))
	end

	local count = countIngredient(rname, 'pipe')
	if rname ~= 'pipe-to-ground' and count > 0 then
		addIngredient(rname, 'gasket', math.max(1, math.ceil(count/2)))
	end

	if isType(rname, 'furnace') and isElectric(rname, 'furnace') then
		addIngredient(rname, 'glass-plate', 5)
	end

	if isType(rname, 'solar-panel')
		or isType(rname, 'car')
	then
		addIngredient(rname, 'glass-plate', 3)
	end

	if isType(rname, 'lamp')
		or isType(rname, 'rail-signal')
		or isType(rname, 'rail-chain-signal')
		or isType(rname, 'night-vision-equipment')
		or isType(rname, 'solar-panel-equipment')
		or isType(rname, 'solar-panel-equipment')
	then
		addIngredient(rname, 'glass-plate', 1)
	end
end

subIngredient('burner-mining-drill', 'copper-bushing',
	countIngredient('burner-mining-drill', 'copper-bushing')
)

addIngredient('automation-science-pack', 'glass-flask', 1)
addIngredient('logistic-science-pack', 'glass-flask', 1)
addIngredient('military-science-pack', 'glass-flask', 1)
addIngredient('chemical-science-pack', 'glass-flask', 1)
addIngredient('production-science-pack', 'glass-flask', 1)
addIngredient('utility-science-pack', 'glass-flask', 1)

addIngredient('repair-pack', 'gasket', 1)
addIngredient('electronic-circuit', 'ceramic-substrate', 1)

repIngredient('concrete', 'iron-ore', 'iron-stick')

local function addDep(tname, dname)
	if data.raw.technology[tname] then
		local tech = data.raw.technology[tname]
		if not tech.prerequisites then
			tech.prerequisites = {}
		end
		table.insert(tech.prerequisites, dname)
	end
end

local function addEffect(tname, rname)
	local tech = data.raw.technology[tname]
	if tech then
		tech.effects = tech.effects or {}
		table.insert(tech.effects, { type = 'unlock-recipe', recipe = rname })
	end
end

addEffect('electronics', 'button-cell')
addIngredient('advanced-circuit', 'button-cell', 1)

addIngredient('processing-unit', 'gold-foil', 1)
addDep('advanced-electronics-2', 'gold-processing')

addIngredient('low-density-structure', 'gold-foil', 5)
addDep('low-density-structure', 'gold-processing')

addIngredient('rocket-control-unit', 'glass-plate', 2)

addDep('rail-signals', 'electronics')
addIngredient('rail-signal', 'button-cell', 1)
addIngredient('rail-chain-signal', 'button-cell', 1)
addIngredient('constant-combinator', 'button-cell', 1)

addIngredient('accumulator', 'power-switch', 1)

addIngredient('electric-engine-unit', 'copper-cable', 4)
